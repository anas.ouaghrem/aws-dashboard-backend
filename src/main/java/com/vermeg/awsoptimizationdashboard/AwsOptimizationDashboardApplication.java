package com.vermeg.awsoptimizationdashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsOptimizationDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsOptimizationDashboardApplication.class, args);
	}

}
