package com.vermeg.awsoptimizationdashboard.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EbsDTO {

    private Map<String, String> tags;
    private String associatedAccount;

    private String volumeId;
    private String volumeType;
    private Date creationTime;
    private String availabilityZone;
    private long size;
    private String state;
    private String productId;
    private String ownerEmail;
    private String operationHours;
    private String environmentType;
}
