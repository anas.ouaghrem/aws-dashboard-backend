package com.vermeg.awsoptimizationdashboard.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RdsDTO {

    private Map<String, String> tags;
    private String associatedAccount;
    private String dbInstanceIdentifier;
    private String dbInstanceClass;
    private String status;
    private String engine;
    private String engineVersion;
    private String region;
    private String ownerEmail;
    private String productId;
    private String operationHours;
    private String clientName;
    private int allocatedStorage;
    private String endpointAddress;
    private int endpointPort;
}
