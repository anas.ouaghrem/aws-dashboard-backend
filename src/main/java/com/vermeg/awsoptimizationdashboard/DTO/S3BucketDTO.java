package com.vermeg.awsoptimizationdashboard.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class S3BucketDTO {
    private String name;
    private Date creationDate;
    private String ownerName;
    private String region;
    private int objectCount;
    private long size;
    private Map<String, String> tags;
    private String associatedAccount;
}

