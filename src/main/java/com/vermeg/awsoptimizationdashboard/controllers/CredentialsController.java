package com.vermeg.awsoptimizationdashboard.controllers;

import com.vermeg.awsoptimizationdashboard.entities.AwsAccountCredentials;
import com.vermeg.awsoptimizationdashboard.services.CredentialsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@CrossOrigin("*")
public class CredentialsController {

    private final CredentialsService awsService;

    @PostMapping("add-account-credentials")
    public AwsAccountCredentials addCredentials(AwsAccountCredentials credentials) {
        return this.awsService.createCredentials(credentials);
    }

    @PutMapping("update-account-credentials")
    public AwsAccountCredentials updateCredentials(Long credentialsId, String name, String accessKeyId, String secretAccessKey){
        return this.awsService.updateCredentials(credentialsId,name,accessKeyId,secretAccessKey);
    }

    @DeleteMapping("delete-account-credentials/{credentialsId}")
    public void deleteCredentials(@PathVariable Long credentialsId){
        this.awsService.deleteCredentials(credentialsId);
    }


    @GetMapping("get-account-credentials/{name}")
    public AwsAccountCredentials getCredentialsByName(@PathVariable String name){
        return this.awsService.getCredentialsByName(name);
    }

    @GetMapping("get-account-credentials")
    public AwsAccountCredentials getCredentials(@PathVariable Long id){
        return this.awsService.getCredentialsById(id);
    }

    @GetMapping("test-connection/{id}")
    public boolean testConnection(@PathVariable Long id){
        return this.awsService.testAwsConnection(id);
    }

}
