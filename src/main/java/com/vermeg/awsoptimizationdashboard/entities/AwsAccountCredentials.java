package com.vermeg.awsoptimizationdashboard.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AwsAccountCredentials {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String accessKeyId;

    private String secretAccessKey;

    @Column(nullable = true)
    private String accountName;

    private String accountId;

    private String region;

    private Date date = new Date();

    private boolean verified = false;

}