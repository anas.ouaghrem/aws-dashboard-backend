package com.vermeg.awsoptimizationdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class EBSVolume {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String associatedAccount;

    private String volumeId;
    private String volumeType;
    private Date creationTime;
    private String availabilityZone;
    private long size;
    private String state;
    private String productId;
    private String ownerEmail;
    private String operationHours;
    private String environmentType;
}
