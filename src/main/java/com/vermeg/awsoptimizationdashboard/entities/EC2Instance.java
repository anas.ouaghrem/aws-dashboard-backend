package com.vermeg.awsoptimizationdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EC2Instance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String associatedAccount;

    private String instanceId;
    private String instanceType;
    private String platform;
    private String region;
    private String state;
    private String publicIp;
    private String privateIp;
    private String productId;
    private String ownerEmail;
    private String environmentType;
    private String operationHours;
    private String ClientName;
}
