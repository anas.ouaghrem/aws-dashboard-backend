package com.vermeg.awsoptimizationdashboard.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EC2OptimizationSuggestion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String associatedAccount;
    private String title;
    private String description;
    private Date createdDate;
    private SuggestionStatus status;
    @ManyToOne
    private EC2Instance linkedInstance;
}
