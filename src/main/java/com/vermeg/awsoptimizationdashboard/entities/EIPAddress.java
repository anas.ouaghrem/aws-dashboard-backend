package com.vermeg.awsoptimizationdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class EIPAddress  {
    @Id
    private Long id;
    private String associatedAccount;

    private String allocationId;
    private String associationId;
    private String publicIp;
    private String privateIp;
    private String instanceId;
}
