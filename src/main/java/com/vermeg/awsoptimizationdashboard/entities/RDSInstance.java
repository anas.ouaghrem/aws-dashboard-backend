package com.vermeg.awsoptimizationdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class RDSInstance  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String associatedAccount;
    private String dbInstanceIdentifier;
    private String dbInstanceClass;
    private String status;
    private String engine;
    private String engineVersion;
    private String region;
    private String ownerEmail;
    private String productId;
    private String operationHours;
    private String clientName;
    private int allocatedStorage;
    private String endpointAddress;
    private int endpointPort;
}
