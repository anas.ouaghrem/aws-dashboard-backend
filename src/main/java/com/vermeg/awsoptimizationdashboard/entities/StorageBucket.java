package com.vermeg.awsoptimizationdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StorageBucket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String associatedAccount;

    private String name;
    private String region;
    private Date creationDate;
    private String owner;
    private int objectCount;
    private long size;
    private String ownerEmail;
    private String purpose;
}
