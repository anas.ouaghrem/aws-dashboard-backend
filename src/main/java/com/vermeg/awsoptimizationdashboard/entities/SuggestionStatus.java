package com.vermeg.awsoptimizationdashboard.entities;

public enum SuggestionStatus {
    Pending,
    Ignored,
    Done,
}
