package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsoptimizationdashboard.entities.AwsAccountCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AwsCredentialsRepository extends JpaRepository<AwsAccountCredentials,Long> {
    AwsAccountCredentials findAwsAccountCredentialsByAccountName(String accountName);
    AwsAccountCredentials findAwsAccountCredentialsByAccessKeyId(String accessKeyId);
}
