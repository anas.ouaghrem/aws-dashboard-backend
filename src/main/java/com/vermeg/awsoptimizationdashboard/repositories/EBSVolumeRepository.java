package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsmultidashboard.models.EBSVolume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EBSVolumeRepository extends JpaRepository<EBSVolume,Long> {
}
