package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsmultidashboard.models.EC2Instance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EC2InstanceRepository extends JpaRepository<EC2Instance,Long> {
}
