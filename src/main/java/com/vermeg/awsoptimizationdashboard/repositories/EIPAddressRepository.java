package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsmultidashboard.models.EIPAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EIPAddressRepository extends JpaRepository<EIPAddress,Long> {
}
