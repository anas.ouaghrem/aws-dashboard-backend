package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsoptimizationdashboard.entities.EBSOptimizationSuggestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EbsOptimizationSuggestionRepository extends JpaRepository<EBSOptimizationSuggestion,Long> {
}
