package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsoptimizationdashboard.entities.EIPOptimizationSuggestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EipOptimizationSuggestionRepository extends JpaRepository<EIPOptimizationSuggestion,Long> {
}
