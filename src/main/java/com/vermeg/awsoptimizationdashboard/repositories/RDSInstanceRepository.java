package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsmultidashboard.models.RDSInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RDSInstanceRepository extends JpaRepository<RDSInstance, Long> {
}
