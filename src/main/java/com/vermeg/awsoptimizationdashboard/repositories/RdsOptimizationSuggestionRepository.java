package com.vermeg.awsoptimizationdashboard.repositories;

import com.vermeg.awsoptimizationdashboard.entities.RDSOptimizationSuggestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RdsOptimizationSuggestionRepository extends JpaRepository<RDSOptimizationSuggestion,Long> {
}
