package com.vermeg.awsoptimizationdashboard.repositories;


import com.vermeg.awsoptimizationdashboard.entities.StorageBucket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageBucketRepository extends JpaRepository<StorageBucket,Long> {

    public StorageBucket findByName(String name);
}
