package com.vermeg.awsoptimizationdashboard.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.vermeg.awsoptimizationdashboard.entities.AwsAccountCredentials;
import com.vermeg.awsoptimizationdashboard.repositories.AwsCredentialsRepository;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class CredentialsService {

    private final AwsCredentialsRepository awsCredentialsRepository;

    public AwsAccountCredentials createCredentials(AwsAccountCredentials credentials) {
        return awsCredentialsRepository.save(credentials);
    }

    public AwsAccountCredentials updateCredentials(Long credentialsId, String name, String accessKeyID, String secretAccessKey){
        AwsAccountCredentials credentials = awsCredentialsRepository.findById(credentialsId).orElseThrow( () -> new RuntimeException("Invalid AWS credentials ID"));
        credentials.setAccountName(name);
        credentials.setAccessKeyId(accessKeyID);
        credentials.setSecretAccessKey(secretAccessKey);
        return awsCredentialsRepository.save(credentials);
    }

    public AwsAccountCredentials getCredentialsById(Long credentialsId){
        return awsCredentialsRepository.findById(credentialsId).orElseThrow( () -> new RuntimeException("Invalid AWS credentials ID"));
    }

    public AwsAccountCredentials getCredentialsByName(String name){
        return awsCredentialsRepository.findAwsAccountCredentialsByAccountName(name);
    }

    public List<AWSCredentials> getAllCredentials(){
        List<AwsAccountCredentials> credentials = awsCredentialsRepository.findAll();
        List<AWSCredentials> result = new ArrayList<>();
        for (AwsAccountCredentials credential : credentials){
            result.add(new BasicAWSCredentials(credential.getAccessKeyId(), credential.getSecretAccessKey()));
        }
        return result;
    }

    public void deleteCredentials(Long credentialsID){
        awsCredentialsRepository.deleteById(credentialsID);
    }

    public AWSCredentials getAwsCredentials(Long awsCredentialsId){
        AwsAccountCredentials credentials = awsCredentialsRepository.findById(awsCredentialsId).orElse(null);
        if (credentials == null) {
            // Credentials not found in the database
            throw new IllegalArgumentException("Invalid AWS credentials ID");
        }
        return new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey());
    }

    public boolean testAwsConnection(Long awsCredentialsId) {

        AwsAccountCredentials credentials = awsCredentialsRepository.findById(awsCredentialsId).orElse(null);
        if (credentials == null) {
            throw new IllegalArgumentException("Invalid AWS credentials ID");
        }

        AWSCredentials awsCredentials = new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey());

        AmazonS3Client s3Client = new AmazonS3Client(awsCredentials);
        s3Client.setRegion(Region.getRegion(Regions.EU_WEST_1));

        try {
            List<Bucket> buckets = s3Client.listBuckets();
            credentials.setVerified(true);
            awsCredentialsRepository.save(credentials);
            return true;
        } catch (AmazonS3Exception e) {
            return false;
        }
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void deleteUnverifiedCredentials(){
        List<AwsAccountCredentials> credentialsList = awsCredentialsRepository.findAll();
        for (AwsAccountCredentials credentials : credentialsList) {
            if(Duration.between( credentials.getDate().toInstant(), new Date().toInstant()).toDays() >= 7 && !credentials.isVerified()) {
                awsCredentialsRepository.delete(credentials);
            }
        }
    }
}
