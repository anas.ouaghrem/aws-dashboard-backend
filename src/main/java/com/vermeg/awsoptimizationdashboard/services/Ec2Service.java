package com.vermeg.awsoptimizationdashboard.services;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;
import com.vermeg.awsoptimizationdashboard.entities.AwsAccountCredentials;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class Ec2Service {

    private UtilityServices utilityServices;
    private CredentialsService awsService;

    public List<Instance> getAllEC2Instances(){
        List<AmazonEC2> clients = utilityServices.getEc2ClientsList();

        List<Instance> instances = new ArrayList<>();
        for (AmazonEC2 client : clients) {
            DescribeInstancesRequest request = new DescribeInstancesRequest();
            DescribeInstancesResult result = client.describeInstances(request);
            List<Reservation> reservations = result.getReservations();
            for (Reservation reservation : reservations) {
                instances.addAll(reservation.getInstances());
            }
        }
        return instances;
    }

    @Cacheable("all-ebs-volumes")
    public List<Volume> getAllVolumes(){
        List<AmazonEC2> clients = utilityServices.getEc2ClientsList();
        List<Volume> volumes = new ArrayList<>();
        for (AmazonEC2 client : clients) {
            DescribeVolumesRequest request = new DescribeVolumesRequest();
            DescribeVolumesResult result = client.describeVolumes(request);
            volumes.addAll(result.getVolumes());
        }
        return volumes;
    }

    @Cacheable("all-eip-addresses")
    public List<Address> getAllEIPAddresses(){
        List<AmazonEC2> clients = utilityServices.getEc2ClientsList();
        List<Address> addresses = new ArrayList<>();
        for (AmazonEC2 client : clients) {
            DescribeAddressesRequest request = new DescribeAddressesRequest();
            DescribeAddressesResult result = client.describeAddresses(request);
            addresses.addAll(result.getAddresses());
        }
        return addresses;
    }

    public List<Instance> listEC2InstancesByAccountAndRegion(String accountName,String region) {
        AwsAccountCredentials credentials = awsService.getCredentialsByName(accountName);
        AmazonEC2 ec2Client = AmazonEC2ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(),credentials.getSecretAccessKey())))
                .withRegion(region)
                .build();
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        DescribeInstancesResult result = ec2Client.describeInstances(request);
        List<Reservation> reservations = result.getReservations();
        List<Instance> instances = new ArrayList<>();

        for (Reservation reservation : reservations) {
            instances.addAll(reservation.getInstances());
        }

        return instances;
    }

    public List<Volume> listEBSVolumesByAccountAndRegion(String accountName,String region) {
        AwsAccountCredentials credentials = awsService.getCredentialsByName(accountName);
        AmazonEC2 ec2Client = AmazonEC2ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(),credentials.getSecretAccessKey())))
                .withRegion(region)
                .build();
        DescribeVolumesRequest request = new DescribeVolumesRequest();
        DescribeVolumesResult result = ec2Client.describeVolumes(request);
        List<Volume> volumes = result.getVolumes();

        return volumes;
    }

    @Cacheable(value = "eip-addresses", key = "accountName")
    public List<Address> listElasticIPsByAccountAndRegion(String accountName,String region) {
        AwsAccountCredentials credentials = awsService.getCredentialsByName(accountName);
        AmazonEC2 ec2Client = AmazonEC2ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(),credentials.getSecretAccessKey())))
                .withRegion(region)
                .build();
        DescribeAddressesRequest request = new DescribeAddressesRequest();
        DescribeAddressesResult result = ec2Client.describeAddresses(request);
        List<Address> elasticIPs = result.getAddresses();

        return elasticIPs;
    }

    public Instance getEC2InstanceByAccountAndRegion(String instanceID,String accountName,String region){
        AwsAccountCredentials credentials = awsService.getCredentialsByName(accountName);
        AmazonEC2 ec2Client = AmazonEC2ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(),credentials.getSecretAccessKey())))
                .withRegion(region)
                .build();
        Instance instance = new Instance();
        List<Instance> instances = new ArrayList<>();
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        DescribeInstancesResult result = ec2Client.describeInstances();
        List<Reservation> reservations = result.getReservations();
        for (Reservation reservation : reservations){
            instances.addAll(reservation.getInstances());


        }

        for (Instance instance1 : instances){
            if(instance1.getInstanceId().equals(instanceID)){
                instance = instance1;

            }
        }
        return instance;
    }


}
