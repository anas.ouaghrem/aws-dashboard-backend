package com.vermeg.awsoptimizationdashboard.services;

import com.amazonaws.services.ec2.model.Address;
import com.vermeg.awsoptimizationdashboard.entities.EBSOptimizationSuggestion;
import com.vermeg.awsoptimizationdashboard.entities.EC2OptimizationSuggestion;
import com.vermeg.awsoptimizationdashboard.entities.EIPOptimizationSuggestion;
import com.vermeg.awsoptimizationdashboard.entities.SuggestionStatus;
import com.vermeg.awsoptimizationdashboard.repositories.EbsOptimizationSuggestionRepository;
import com.vermeg.awsoptimizationdashboard.repositories.Ec2OptimizationSuggestionRepository;
import com.vermeg.awsoptimizationdashboard.repositories.EipOptimizationSuggestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class Ec2SuggestionsService {

    // Local Variables

    private final Ec2OptimizationSuggestionRepository ec2OptimizationSuggestionRepository;
    private final EbsOptimizationSuggestionRepository ebsOptimizationSuggestionRepository;
    private final EipOptimizationSuggestionRepository eipOptimizationSuggestionRepository;
    private final Ec2Service ec2Service;

    // EIP Optimization Suggestions

    public List<EIPOptimizationSuggestion> generateEipSuggestions(){
        List<Address> addresses = ec2Service.getAllEIPAddresses();
        List<EIPOptimizationSuggestion> suggestions = new ArrayList<>();
        for (Address address : addresses){
            if(address.getInstanceId() == null) {
                EIPOptimizationSuggestion suggestion = new EIPOptimizationSuggestion();
                suggestion.setTitle("EIP not associated with an instance");
                suggestion.setDescription("Associate the EIP with an instance");
                suggestion.setStatus(SuggestionStatus.Pending);
                suggestion.setCreatedDate(new Date());
                suggestion.setAssociatedAccount(null);
                suggestions.add(suggestion);
            }
        }
        return eipOptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<EIPOptimizationSuggestion> getAllEipSuggestions(){
        return eipOptimizationSuggestionRepository.findAll();
    }

    public EIPOptimizationSuggestion getEipSuggestionById(Long id){
        return eipOptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public EIPOptimizationSuggestion updateEipSuggestion(SuggestionStatus status, Long id){
        EIPOptimizationSuggestion suggestion = eipOptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return eipOptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteEipSuggestion(Long id){
        eipOptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithEipSuggestions(){
        List<EIPOptimizationSuggestion> suggestions = eipOptimizationSuggestionRepository.findAll();
        for (EIPOptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending){
                eipOptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }

    // EC2 Optimization Suggestions
    public List<EC2OptimizationSuggestion> generateEc2Suggestions(){
        List<EC2OptimizationSuggestion> suggestions = new ArrayList<>();
        return ec2OptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<EC2OptimizationSuggestion> getAllEc2Suggestions(){
        return ec2OptimizationSuggestionRepository.findAll();
    }

    public EC2OptimizationSuggestion getEc2SuggestionById(Long id){
        return ec2OptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public EC2OptimizationSuggestion updateEc2Suggestion(SuggestionStatus status, Long id){
        EC2OptimizationSuggestion suggestion = ec2OptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return ec2OptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteEc2Suggestion(Long id){
        ec2OptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithEc2Suggestions(){
        List<EC2OptimizationSuggestion> suggestions = ec2OptimizationSuggestionRepository.findAll();
        for (EC2OptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending){
                ec2OptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }


    // EBS Optimization Suggestions

    public List<EBSOptimizationSuggestion> generateEbsSuggestions(){
        List<EBSOptimizationSuggestion> suggestions = new ArrayList<>();
        return ebsOptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<EBSOptimizationSuggestion> getAllEbsSuggestions(){
        return ebsOptimizationSuggestionRepository.findAll();
    }

    public EBSOptimizationSuggestion getEbsSuggestionById(Long id){
        return ebsOptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public EBSOptimizationSuggestion updateEbsSuggestion(SuggestionStatus status, Long id){
        EBSOptimizationSuggestion suggestion = ebsOptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return ebsOptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteEbsSuggestion(Long id){
        ebsOptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithEbsSuggestions(){
        List<EBSOptimizationSuggestion> suggestions = ebsOptimizationSuggestionRepository.findAll();
        for (EBSOptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending){
                ebsOptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }
}
