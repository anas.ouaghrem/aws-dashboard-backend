package com.vermeg.awsoptimizationdashboard.services;

import com.vermeg.awsoptimizationdashboard.entities.RDSOptimizationSuggestion;
import com.vermeg.awsoptimizationdashboard.entities.SuggestionStatus;
import com.vermeg.awsoptimizationdashboard.repositories.RdsOptimizationSuggestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RdsSuggestionsService {

    // Local Variable Declaration

    private final RdsOptimizationSuggestionRepository rdsOptimizationSuggestionRepository;
    private final RdsService rdsService;

    // Method Declaration

    public List<RDSOptimizationSuggestion> generateRdsSuggestions() {
        return null;
    }

    public List<RDSOptimizationSuggestion> getAllRdsSuggestions() {
        return rdsOptimizationSuggestionRepository.findAll();
    }

    public RDSOptimizationSuggestion getRdsSuggestionById(Long id) {
        return rdsOptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public RDSOptimizationSuggestion updateRdsSuggestionStatus(SuggestionStatus status, Long id) {
        RDSOptimizationSuggestion suggestion = rdsOptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return rdsOptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteRdsSuggestion(Long id) {
        rdsOptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithSuggestions() {
        List<RDSOptimizationSuggestion> suggestions = rdsOptimizationSuggestionRepository.findAll();
        for (RDSOptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending){
                rdsOptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }
}
