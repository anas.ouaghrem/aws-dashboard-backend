package com.vermeg.awsoptimizationdashboard.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.macie2.model.S3Bucket;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.vermeg.awsoptimizationdashboard.DTO.S3BucketDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class S3Service {

    // Local Variables Declaration
    private final UtilityServices utilityServices;
    private final CredentialsService credentialsService;

    private final AmazonS3 s3Client;

    List<Regions> awsRegions = Arrays.asList(
            Regions.EU_WEST_2,
//            Regions.US_EAST_1,
            Regions.US_EAST_2,
            Regions.EU_WEST_1,
            Regions.EU_WEST_3,
            Regions.AP_NORTHEAST_1,
            Regions.AP_NORTHEAST_2,
            Regions.AP_SOUTH_1,
            Regions.AP_SOUTHEAST_1,
            Regions.AP_SOUTHEAST_2,
            Regions.CA_CENTRAL_1

    );

    @Cacheable("ec2-clients")
    public List<AmazonS3> getS3ClientsList() {
        List<AmazonS3> s3ClientsList = new ArrayList<>();

        for (AWSCredentials credentials : this.credentialsService.getAllCredentials()) {

            for (Regions region : awsRegions) {
                s3ClientsList.add(AmazonS3ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build());
            }
        }
        return s3ClientsList;
    }

    @Autowired
    public S3Service(CredentialsService awsService) {
        this.credentialsService = awsService;
        this.s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(this.credentialsService.getAwsCredentials(Long.valueOf(1L)).getAWSAccessKeyId(), this.credentialsService.getAwsCredentials(Long.valueOf(1L)).getAWSSecretKey())))
                .withRegion(Regions.EU_WEST_1)
                .build();

    }


    public List<S3ObjectSummary> listS3Objects(String bucketName) {
        ObjectListing objectListing = s3Client.listObjects(bucketName);
        return objectListing.getObjectSummaries();
    }

    @Cacheable("buckets")
    public List<S3BucketDTO> getBuckets() {
        List<S3BucketDTO> result = new ArrayList<>();
        List<Bucket> buckets = s3Client.listBuckets();
        List<Bucket> targetBuckets = new ArrayList<>();
        String region = s3Client.getRegion().toAWSRegion().getName();
        for (Bucket bucket : buckets) {
            String bucketRegion = s3Client.getBucketLocation(bucket.getName());
            if (bucketRegion.equals(region)) {
                targetBuckets.add(bucket);
            }
        }

        for (Bucket bucket : targetBuckets) {
            ListObjectsV2Result objectListing = s3Client.listObjectsV2(bucket.getName());
            int objectCount = objectListing.getKeyCount();
            long size = 0;
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                size += objectSummary.getSize();
            }

            Map<String, String> tags = new HashMap<>();
            BucketTaggingConfiguration bucketTagging = s3Client.getBucketTaggingConfiguration(bucket.getName());
            if (bucketTagging != null) {
                List<TagSet> tagSetList = bucketTagging.getAllTagSets();
                if (tagSetList != null) {
                    for (TagSet tagSet : tagSetList) {
                        Map<String, String> tagMap = tagSet.getAllTags();
                        if (tagMap != null) {
                            tags.putAll(tagMap);
                        }
                    }
                }
            }
            S3BucketDTO s3Bucket = new S3BucketDTO(bucket.getName(), bucket.getCreationDate(), bucket.getOwner().getDisplayName(), region, objectCount, size, tags, );
            result.add(s3Bucket);
        }

        return result;
    }


    @Cacheable("s3test")
    public List<S3BucketDTO> s3test() {
        List<Bucket> buckets = s3Client.listBuckets();
        String region = s3Client.getRegion().toAWSRegion().getName();

        List<S3BucketDTO> result = buckets.parallelStream()
                .filter(bucket -> s3Client.getBucketLocation(bucket.getName()).equals(region))
                .flatMap(bucket -> {
                    ListObjectsV2Result objectListing = s3Client.listObjectsV2(bucket.getName());
                    int objectCount = objectListing.getKeyCount();
                    long size = objectListing.getObjectSummaries().stream()
                            .mapToLong(S3ObjectSummary::getSize).sum();

                    Map<String, String> tags = new HashMap<>();
                    BucketTaggingConfiguration bucketTagging = s3Client.getBucketTaggingConfiguration(bucket.getName());
                    if (bucketTagging != null) {
                        List<TagSet> tagSetList = bucketTagging.getAllTagSets();
                        if (tagSetList != null) {
                            for (TagSet tagSet : tagSetList) {
                                Map<String, String> tagMap = tagSet.getAllTags();
                                if (tagMap != null) {
                                    tags.putAll(tagMap);
                                }
                            }
                        }
                    }

                    S3BucketDTO s3Bucket = new S3BucketDTO(bucket.getName(), bucket.getCreationDate(), bucket.getOwner().getDisplayName(), region, objectCount, size, tags);
                    return Stream.of(s3Bucket);
                })
                .collect(Collectors.toList());

        return result;

    }


    @Cacheable("s3testAll")
    public List<S3BucketDTO> s3testAllRegions() throws Exception {

        List<Bucket> buckets = s3Client.listBuckets();
        //List<AmazonS3> s3ClientsList = getS3ClientsList();
        List<AmazonS3> s3ClientsList = new ArrayList<>();
        for (Regions region : awsRegions) {
            s3ClientsList.add(AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(this.credentialsService.getCredentialsById(1L).getAccessKeyId(), this.credentialsService.getCredentialsById(1L).getSecretAccessKey())))
                    .withRegion(region)
                    .build());
        }

        List<S3BucketDTO> resultBuckets = new ArrayList<>();

        for (AmazonS3 client : s3ClientsList) {

            String region = client.getRegionName();
            System.out.println(region);

            for (Bucket bucket : buckets) {
                String bucketRegion = client.getBucketLocation(bucket.getName());
                if (bucketRegion == null || bucketRegion.isEmpty()) {
                    System.out.println("Bucket " + bucket.getName() + " has an empty location, skipping...");
                    continue;
                }
                if (bucketRegion.equals(region)) {
                    ListObjectsV2Result objectListing = client.listObjectsV2(bucket.getName());
                    int objectCount = objectListing.getKeyCount();
                    long size = 0;
                    for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                        size += objectSummary.getSize();
                    }

                    Map<String, String> tags = new HashMap<>();
                    BucketTaggingConfiguration bucketTagging = client.getBucketTaggingConfiguration(bucket.getName());
                    if (bucketTagging != null) {
                        List<TagSet> tagSetList = bucketTagging.getAllTagSets();
                        if (tagSetList != null) {
                            for (TagSet tagSet : tagSetList) {
                                Map<String, String> tagMap = tagSet.getAllTags();
                                if (tagMap != null) {
                                    tags.putAll(tagMap);
                                }
                            }
                        }
                    }
                    S3BucketDTO s3Bucket = new S3BucketDTO(bucket.getName(), bucket.getCreationDate(), bucket.getOwner().getDisplayName(), region, objectCount, size, tags);

                    resultBuckets.add(s3Bucket);
                }
            }
        }

        return resultBuckets;
    }




}
