package com.vermeg.awsoptimizationdashboard.services;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.amazonaws.services.s3.model.GetBucketLifecycleConfigurationRequest;
import com.vermeg.awsoptimizationdashboard.DTO.S3BucketDTO;
import com.vermeg.awsoptimizationdashboard.entities.AwsAccountCredentials;
import com.vermeg.awsoptimizationdashboard.entities.S3OptimizationSuggestion;
import com.vermeg.awsoptimizationdashboard.entities.StorageBucket;
import com.vermeg.awsoptimizationdashboard.entities.SuggestionStatus;
import com.vermeg.awsoptimizationdashboard.repositories.S3OptimizationSuggestionRepository;
import com.vermeg.awsoptimizationdashboard.repositories.StorageBucketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class S3SuggestionsService {

    // Local variables

    private final S3OptimizationSuggestionRepository s3OptimizationSuggestionRepository;
    private final StorageBucketRepository storageBucketRepository;
    private final CredentialsService credentialsService;
    private final S3Service s3Service;

    // Methods

    public List<S3OptimizationSuggestion> generateS3Suggestions() {

        List<S3BucketDTO> buckets = s3Service.getBuckets();
        for (String bucketName : bucketNames) {
            if (!s3Service.hasLifecyclePolicy(bucketName)) {
                S3OptimizationSuggestion suggestion = S3OptimizationSuggestion.builder()
                        .title("Add Lifecycle Policy to Bucket")
                        .description("Bucket " + bucketName + " does not have a lifecycle policy. Consider adding one to " +
                                "automatically transition objects to lower-cost storage classes or delete them when they " +
                                "are no longer needed.")
                        .createdDate(new Date())
                        .status(SuggestionStatus.NEW)
                        .associatedAccount(s3Service.getBucketAssociatedAccount(bucketName))
                        .build();
                suggestions.add(suggestion);
            }
        }

        return suggestions;
    }

    public List<S3OptimizationSuggestion> getAllS3Suggestions() {
        return s3OptimizationSuggestionRepository.findAll();
    }

    public S3OptimizationSuggestion getS3SuggestionById(Long id) {
        return s3OptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public S3OptimizationSuggestion updateS3SuggestionStatus(SuggestionStatus status, Long id) {
        S3OptimizationSuggestion suggestion = s3OptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return s3OptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteS3Suggestion(Long id) {
        s3OptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithSuggestions() {
        List<S3OptimizationSuggestion> suggestions = s3OptimizationSuggestionRepository.findAll();
        for (S3OptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending){
                s3OptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }

    public boolean hasLifecyclePolicy(String bucketName) {
        StorageBucket bucket = storageBucketRepository.findByName(bucketName);
        AwsAccountCredentials credentials = credentialsService.getCredentialsByName(bucket.getAssociatedAccount());

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey())))
                .withRegion(bucket.getRegion())
                .build();

        GetBucketLifecycleConfigurationRequest request = new GetBucketLifecycleConfigurationRequest(bucketName);
        BucketLifecycleConfiguration configuration = s3Client.getBucketLifecycleConfiguration(request);

        return configuration != null && configuration.getRules().size() > 0;
    }
}
