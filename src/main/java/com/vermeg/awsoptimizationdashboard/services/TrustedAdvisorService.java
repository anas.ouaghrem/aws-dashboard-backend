package com.vermeg.awsoptimizationdashboard.services;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.support.AWSSupport;
import com.amazonaws.services.support.AWSSupportClient;
import com.amazonaws.services.support.model.*;
import com.vermeg.awsoptimizationdashboard.entities.AwsAccountCredentials;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class TrustedAdvisorService {
    private final CredentialsService awsService;

    public List<TrustedAdvisorCheckDescription> getTrustedAdvisorCheckSummary(String accountName, String region) {
        AwsAccountCredentials credentials = awsService.getCredentialsByName(accountName);
        AWSSupport supportClient = AWSSupportClient.builder()
                .withCredentials(new AWSStaticCredentialsProvider( new BasicAWSCredentials(credentials.getAccessKeyId(),credentials.getSecretAccessKey())))
                .withRegion(region)
                .build();
        DescribeTrustedAdvisorChecksRequest request = new DescribeTrustedAdvisorChecksRequest().withLanguage("en");
        DescribeTrustedAdvisorChecksResult result = supportClient.describeTrustedAdvisorChecks(request);
        List<TrustedAdvisorCheckDescription> summaries = result.getChecks();

        return summaries;
    }


    public TrustedAdvisorCheckResult getTrustedAdvisorCheckResult(String checkId, String accountName, String region) {
        AwsAccountCredentials credentials = awsService.getCredentialsByName(accountName);
        AWSSupport supportClient = AWSSupportClient.builder()
                .withCredentials(new AWSStaticCredentialsProvider( new BasicAWSCredentials(credentials.getAccessKeyId(),credentials.getSecretAccessKey())))
                .withRegion(region)
                .build();
        DescribeTrustedAdvisorCheckResultRequest request = new DescribeTrustedAdvisorCheckResultRequest();
        request.setCheckId(checkId);
        DescribeTrustedAdvisorCheckResultResult result = supportClient.describeTrustedAdvisorCheckResult(request);
        TrustedAdvisorCheckResult checkResult = result.getResult();

        return checkResult;
    }

    public List<TrustedAdvisorCheckSummary> getTrustedAdvisorCheckSummaries(String checkId,String accountName, String region) {
        AwsAccountCredentials credentials = awsService.getCredentialsByName(accountName);
        AWSSupport supportClient = AWSSupportClient.builder()
                .withCredentials(new AWSStaticCredentialsProvider( new BasicAWSCredentials(credentials.getAccessKeyId(),credentials.getSecretAccessKey())))
                .withRegion(region)
                .build();
        DescribeTrustedAdvisorCheckSummariesRequest request = new DescribeTrustedAdvisorCheckSummariesRequest();
        request.setCheckIds(Arrays.asList(checkId));
        DescribeTrustedAdvisorCheckSummariesResult result = supportClient.describeTrustedAdvisorCheckSummaries(request);
        List<TrustedAdvisorCheckSummary> summaries = result.getSummaries();

        return summaries;
    }
}