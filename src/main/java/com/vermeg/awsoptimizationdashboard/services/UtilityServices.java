package com.vermeg.awsoptimizationdashboard.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.Address;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.Volume;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.GetAccountSummaryRequest;
import com.amazonaws.services.identitymanagement.model.GetAccountSummaryResult;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClientBuilder;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.vermeg.awsoptimizationdashboard.DTO.EbsDTO;
import com.vermeg.awsoptimizationdashboard.DTO.Ec2DTO;
import com.vermeg.awsoptimizationdashboard.DTO.EipDTO;
import com.vermeg.awsoptimizationdashboard.DTO.RdsDTO;
import com.vermeg.awsoptimizationdashboard.entities.AwsAccountCredentials;
import com.vermeg.awsoptimizationdashboard.repositories.AwsCredentialsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UtilityServices {

    private final CredentialsService awsService;
    private final AwsCredentialsRepository awsCredentialsRepository;
    private static final Regions[] awsRegions = {
            Regions.US_EAST_1,     // US East (N. Virginia)
            Regions.US_EAST_2,     // US East (Ohio)
            Regions.EU_WEST_1,     // Europe (Ireland)
            Regions.EU_WEST_2,
            Regions.EU_WEST_3,
            Regions.AP_NORTHEAST_1,
    };

    @Cacheable("ec2-clients")
    public List<AmazonEC2> getEc2ClientsList () {
        List<AmazonEC2> ec2ClientsList = new ArrayList<>();
        for (AWSCredentials credentials : this.awsService.getAllCredentials()){
            for (Regions region : awsRegions) {
                ec2ClientsList.add(AmazonEC2ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(),credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build());
            }
        }
        return ec2ClientsList;
    }

    @Cacheable("rds-clients")
    public List<AmazonRDS> getRDSClientsList () {
        List<AmazonRDS> rdsClientsList = new ArrayList<>();

        for (AWSCredentials credentials : this.awsService.getAllCredentials()){
            for (Regions region : awsRegions) {
                rdsClientsList.add(AmazonRDSClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(),credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build());
            }
        }
        return rdsClientsList;
    }

    @Cacheable("s3-clients")
    public List<AmazonS3> getS3ClientsList() {
        List<AmazonS3> s3ClientsList = new ArrayList<>();

        for (AWSCredentials credentials : this.awsService.getAllCredentials()) {

            for (Regions region : awsRegions) {
                s3ClientsList.add(AmazonS3ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build());
            }
        }
        return s3ClientsList;
    }

    public String getAwsAccountName(String awsAccountKeyID) {

        AwsAccountCredentials credentials = this.awsCredentialsRepository.findAwsAccountCredentialsByAccessKeyId(awsAccountKeyID);

        AmazonIdentityManagement iam = AmazonIdentityManagementClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey())))
                .build();

        String accountAlias = iam.listAccountAliases().getAccountAliases().get(0);
        if (accountAlias != null && !accountAlias.isEmpty()) {
            return accountAlias;
        } else {
            return iam.listUsers().getUsers().get(0).getArn().split(":")[4];
        }
    }

    public Ec2DTO mapToEc2Dto(Instance ec2Instance){
        Map<String, String> tagsMap = new HashMap<>();
        for (Tag tag : ec2Instance.getTags()) {
            tagsMap.put(tag.getKey(), tag.getValue());
        }
        return Ec2DTO.builder()
                .tags(tagsMap)
                .instanceId(ec2Instance.getInstanceId())
                .instanceType(ec2Instance.getInstanceType())
                .platform(ec2Instance.getPlatform())
                .region(ec2Instance.getPlacement().getAvailabilityZone())
                .state(ec2Instance.getState().getName())
                .publicIp(ec2Instance.getPublicIpAddress())
                .privateIp(ec2Instance.getPrivateIpAddress())
                .build();
    }

    public EbsDTO mapToEbsDto(Volume ebsVolume){
        Map<String, String> tagsMap = new HashMap<>();
        for (Tag tag : ebsVolume.getTags()) {
            tagsMap.put(tag.getKey(), tag.getValue());
        }
        return EbsDTO.builder()
                .tags(tagsMap)
                .volumeId(ebsVolume.getVolumeId())
                .volumeType(ebsVolume.getVolumeType())
                .state(ebsVolume.getState())
                .availabilityZone(ebsVolume.getAvailabilityZone())
                .build();
    }

    public EipDTO mapToEipDto(Address eipAddress){
        return EipDTO.builder()
                .allocationId(eipAddress.getAllocationId())
                .publicIp(eipAddress.getPublicIp())
                .privateIp(eipAddress.getPrivateIpAddress())
                .instanceId(eipAddress.getInstanceId())
                .associationId(eipAddress.getAssociationId())
                .build();
    }

    public RdsDTO mapToRdsDto(DBInstance rdsInstance){
        Map<String, String> tagsMap = new HashMap<>();
        for (com.amazonaws.services.rds.model.Tag tag : rdsInstance.getTagList()) {
            tagsMap.put(tag.getKey(), tag.getValue());
        }

        return RdsDTO.builder().build();
    }

    public String getAccountId(String accessKey, String secretKey) {

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        AmazonIdentityManagementClient iamClient = (AmazonIdentityManagementClient) AmazonIdentityManagementClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_1)
                .build();

        GetAccountSummaryRequest request = new GetAccountSummaryRequest();
        GetAccountSummaryResult response = iamClient.getAccountSummary(request);

        return response.getSummaryMap().get("AccountCount").toString();
    }
}
